
<!-- README.md is generated from README.Rmd. Please edit that file -->

Le package **prenomfrance** améliore la connaissance de la démographie
française à partir de ses prénoms. A partir des bases de prenoms *Insee*
actualisées chaque année, ainsi que de la base de données du site
*Behind The name* et des résultats du BAC provenant du ministère de
l’Education Nationale, ce package fournit des outils de visualisation
sur les statistiques des prénoms donnés aux enfants.

Pour lancer l’application R Shiny: run\_prenomfrance()

Une image de l’appli est visible ici:
<https://douxvalkyn.shinyapps.io/prenomfrance/>

L’application est composée de 4 onglets:

\_ Cartographie:

  - carte de la proportion de prénoms donnés aux enfants par origine et
    par département

  - carte de la répartition des origines des prenoms

  - graphe de l’évolution des origines des prénoms

  - graphe de la concentration des prenoms

\_ Popularité:

  - nuage de mots des prénoms

  - top 10 des prénoms (masculin, féminin), dans une plage d’année
    choisie, et par origine

  - graphe de l’évolution de la popularité d’un prénom sélectionné

  - top des prénoms les moins éphémères

  - animation du top 10 de sprénoms (masculin, féminin) depuis 1900

\_ Réseau:

  - pour un prénom sélectionné, le réseau des prénoms voisins, selon la
    distance de Jaro-Winkler

\_ Diplômes:

  - graphique du taux de mention au BAC2019, par prenom ou par origine.
    Possibilité de filtrer selon le type de Bac, la spécialité et
    l’académie.
