# prenomfrance (development version)

# prenomfrance 0.1.4
* Mise à jour des prénoms 2020

# prenomfrance 0.1.3
* Recalcul de la base des résultats au bac 2019 à partir de la base des prénoms enrichie.

# prenomfrance 0.1.2
* Enrichissement des origines des prénoms: 95% des naissances depuis 1900 et 90% des naissances récentes (depuis 2000) ont une origine. 
* Correction des taux par origine en supprimant les prénoms sans origine.

# prenomfrance 0.1.1
* Popularité: ajout d'un choix pour afficher la fréquence des prénoms en plus de l'effectif des prénoms dans le graphique en barres en évolution.

# prenomfrance 0.1.0
* Popularité: ajout graphique top durée de vie des prénoms

# prenomfrance 0.0.6
* Diplômes: ajout d'un classement par prenom en plus d'un classement par origine des prenoms

# prenomfrance 0.0.5
* Diplômes: ajout de l'onglet Diplômes: nuage de points du taux de mention selon l'origine du prénom

# prenomfrance 0.0.4
* Reseau: couleurs des noeuds en fonction du sexe; effectif et origine dans l'infobulle des noeuds.

# prenomfrance 0.0.3
* Ajout onglet Popularité avec nuage des prénoms dynamique et tableaux top10.
* Ajout onglet Reseau: à partir d'un prenom choisi, affichage du reseau des prenoms proches selon la distance de Jaro Winkler.

# prenomfrance 0.0.2
* Ajout graphique de concentration des prénoms et bouton switch entre les deux graphiques.
* Modification de l'interface pour ajouter un bouton radio.

# prenomfrance 0.0.1
* Application fonctionnelle avec deux map en page principale et un graphique en évolution.
* Added a `NEWS.md` file to track changes to the package.
